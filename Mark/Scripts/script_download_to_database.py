from datetime import date
from asset import Asset
from datasources import DatabaseFinancial
from datasources import eikon

# To turn LSE symbols into RICs, one can use the line-by-line code, e.g. for Royal Dutch Shell
# =RSearch("Equity",CONCAT("TickerSymbol:", "RDSA", " ExchangeCode:LSE"), "NBROWS:1")

#ftse100 = ['III.L', 'ADML.L', 'AAL.L', 'ANTO.L', 'AHT.L', 'ABF.L', 'AZN.L', 'AUTOA.L', 'AVV.L', 'AV.L', 'BAES.L', 'BARC.L', 'BDEV.L', 'BKGH.L', 'BHPB.L', 'BP.L', 'BATS.L', 'BLND.L', 'BT.L', 'BNZL.L', 'BRBY.L', 'CCL.L', 'CNA.L', 'CCH.L', 'CPG.L', 'CRH.L', 'CRDA.L', 'DCC.L', 'DGE.L', 'EVRE.L', 'EXPN.L', 'FERG.L', 'FLTRF.L', 'FRES.L', 'GSK.L', 'GLEN.L', 'HLMA.L', 'HRGV.L', 'HSX.L', 'HSBA.L', 'HIK.L', 'IMB.L', 'INF.L', 'IHG.L', 'ICAG.L', 'ITRK.L', 'ITV.L', 'JD.L', 'JMAT.L', 'KGF.L', 'LAND.L', 'LGEN.L', 'LLOY.L', 'LSE.L', 'MNG.L', 'MGGT.L', 'MRON.L', 'MNDI.L', 'MRW.L', 'NG.L', 'NXT.L', 'NMC.L', 'OCDO.L', 'PSON.L', 'PSN.L', 'PHNX.L', 'POLYP.L', 'PRU.L', 'RB.L', 'REL.L', 'RTO.L', 'RIO.L', 'RMV.L', 'RR.L', 'RBS.L', 'RDSa.L', 'RSA.L', 'SGE.L', 'SBRY.L', 'SDR.L', 'SMT.L', 'SGRO.L', 'SVT.L', 'SN.L', 'SMDS.L', 'SMIN.L', 'SKG.L', 'SPX.L', 'SSE.L', 'STAN.L', 'SLA.L', 'SJP.L', 'TW.L', 'TSCO.L', 'TUIT.L', 'ULVR.L', 'UU.L', 'VOD.L', 'WTB.L', 'WPP.L']

rics = ['FLTRF.L', 'DLGD.L']
date_start_new = date(2014, 5, 18)
date_end_new = date(2019, 5, 21)
if date_end_new < date_start_new: raise Exception("Seriously?")

def function(asset, date_start, date_end):
    if date_start >= date_end: return
    prices_unadjusted = eikon.get_historic_prices(asset, date_start, date_end)
    prices_adjusted   = eikon.get_historic_prices(asset, date_start, date_end, adjusted=True)
    adjustments       = Asset.adjustments(prices_unadjusted, prices_adjusted)
    corax_adjustments = eikon.corax_dataframe_processed(asset.ric, date_start, date_end)
    asset.save_prices(prices_unadjusted)
    asset.save_adjustments(adjustments)
    asset.save_corax_adjustments(corax_adjustments)

all_symbology = eikon.get_symbology(symbols=rics, from_symbol_type='RIC')
asset_ids = []
with DatabaseFinancial('historical_prices', 3307) as historical_prices:
    for idx, ric in enumerate(rics):
        asset_id = historical_prices.get_or_create_asset(all_symbology.loc[ric])
        asset_ids.append(asset_id)
    rics = all_symbology['RIC'].to_list() # be careful with using OAPermID because this may pick up a primary listing
    info = eikon.get_company_info(instruments=rics)
    if len(asset_ids) != len(info.index): raise Exception('Something has gone wrong')
    info['id'] = asset_ids; info.set_index('id', inplace=True)
    historical_prices.update_company(info)
    
with DatabaseFinancial('historical_prices', 3307) as historical_prices:
    asset_ids = historical_prices.get_asset_ids(rics=rics)
    for idx, ric in enumerate(rics):
        asset_id = asset_ids[idx]
        if asset_id is None:
            info = eikon.get_company_info(ric)
            if info.loc[ric, 'RIC'] != ric: raise Exception('The ric {} might be invalid'.format(ric))
            historical_prices.save_company(info) 
            asset_id = historical_prices.get_asset_ids(rics=[ric])[0]
            if asset_id is None: raise Exception("The database {} is mangled".format(historical_prices))
    
        asset = Asset(asset_id, historical_prices)
        print(asset.name)
        if asset.start_date is None and asset.end_date is None:
            if   date_end_new < asset.first_trade_date: continue
            if date_start_new < asset.first_trade_date: date_start_new = asset.first_trade_date
            function(asset, date_start_new, date_end_new)
            asset.start_date = date_start_new
            asset.end_date = date_end_new
        else:
            if asset.start_date is None or asset.end_date is None: raise Exception("The database {} is mangled".format(historical_prices))
            if date_start_new < asset.start_date:
                if date_start_new < asset.first_trade_date: date_start_new = asset.first_trade_date
                function(asset, date_start_new, asset.start_date)
                asset.start_date = date_start_new
            if date_end_new > asset.end_date:
                function(asset, asset.end_date, date_end_new)
                asset.end_date = date_end_new
        asset.plot(adjusted=True)
