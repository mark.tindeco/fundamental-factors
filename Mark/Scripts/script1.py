from Mark.FactorModelCalibration import constructFactorMimickingPortfolios, dailyClosePricesFromDatabase
from datasources import DatabaseFinancial
import pandas as pd, numpy as np
from datetime import date

rics = ['AAL.L', 'AZN.L', 'BATS.L', 'BP.L', 'DGE.L', 'GLEN.L', 'GSK.L', 'HSBA.L', 'LLOY.L', 'PRU.L', 'RB.L', 'RDSa.L', 'RDSb.L', 'RIO.L', 'ULVR.L']
start_date, end_date = date(2014, 5, 18), date(2019, 5, 21)

def computeBetas(propertiesDataFile, rics):
    properties = pd.read_csv(propertiesDataFile).set_index('RIC').sort_index()
    caps = properties['Cap']
    weights = caps / caps.sum()
    betas = pd.DataFrame(index=rics)
    sector_names = properties['Sector'].dropna().unique().tolist()
    for sector_name in sector_names: betas[sector_name] = properties['Sector'].apply(lambda sector: 1 if sector == sector_name else 0)[rics]
    divs = properties['Div']
    divs_mean, divs_std = np.average(divs, weights=weights), np.sqrt(np.cov(divs, aweights=weights))
    normalised_divs = (divs - divs_mean) / divs_std
    betas['weightNormalisedDivs'] = normalised_divs
    log_caps = caps.apply(np.log)
    log_cap_mean, log_cap_std  = np.average(log_caps), np.std(log_caps, ddof=1) # np.std(log_caps, ddof=1) = np.sqrt(np.cov(log_caps))
    betas['normalisedLogCaps'] = (log_caps - log_cap_mean) / log_cap_std
    log_pes = properties['P/E_Ratio'].apply(np.log).fillna(0)
    log_pes_mean, log_pes_std = np.average(log_pes), np.std(log_pes, ddof=1)  # np.std(log_pes,  ddof=1) = np.sqrt(np.cov(log_pes))
    betas['normalisedLogPE']  = (log_pes - log_pes_mean) / log_pes_std
    return betas

betas = computeBetas('script1_AssetProperties.csv', rics)

with DatabaseFinancial('historical_prices', 3307) as historical_prices:
    daily_close_prices = dailyClosePricesFromDatabase(historical_prices, rics, start_date, end_date)
    index_close_prices = dailyClosePricesFromDatabase(historical_prices, ['.FTSE'], start_date, end_date).iloc[:, 0]

assetLogExcessReturns = np.log(daily_close_prices.div(index_close_prices, axis='rows')).diff().iloc[1:].sort_index(axis='columns')
assetLogExcessReturnsCovariances = assetLogExcessReturns.cov()
assetLogExcessReturnsCovariancesInverse = pd.DataFrame(np.linalg.inv(assetLogExcessReturnsCovariances.values), index=assetLogExcessReturnsCovariances.index, columns=assetLogExcessReturnsCovariances.columns)

# If we *only* use insustry factors then the factors are comprised of a weighted average of the constituents.
    # If using olsFactors, the loading factors/weights are those of a straight average.
    # If using wlsFactors, the loading factors/weights are associated with the reciprocal of the residual variances of the ordinary least squares.
    # Indeed, wlsFactorLoadings.loc['AAL.L', 'Mining'] = weights['AAL.L'] / weights[['AAL.L', 'GLEN.L', 'RIO.L']].sum()
# For example, Pharma = 0.5 * GSK.L + 0.5 AZN.L
#              Mining = 0.3650544333369001 * AAL.L + 0.4277970388142679 * GLEN.L + 0.2954777994719129 * RIO.L
# More generally, fundamental factors are a linear combination of _all_ asset returns.

# Ordinary Least Squares
identity = pd.DataFrame(np.identity(len(rics)), index=rics, columns=rics)
olsFactorMimickingPortfolios = constructFactorMimickingPortfolios(identity, betas, assetLogExcessReturns)
olsFactorLogExcessReturns = assetLogExcessReturns @ olsFactorMimickingPortfolios
olsResiduals = assetLogExcessReturns - olsFactorLogExcessReturns @ betas.T
olsResidualVariance = olsResiduals.var()
olsRecoveredBetas = assetLogExcessReturnsCovariances @ olsFactorMimickingPortfolios @ np.linalg.inv(olsFactorMimickingPortfolios.T @ assetLogExcessReturnsCovariances @ olsFactorMimickingPortfolios); olsRecoveredBetas.columns = betas.columns

# Weighted Least Squares
wlsWeights = pd.DataFrame(np.diag(np.reciprocal(olsResidualVariance)), index=rics, columns=rics)
wlsFactorMimickingPortfolios = constructFactorMimickingPortfolios(wlsWeights, betas, assetLogExcessReturns)
wlsRecoveredBetas = assetLogExcessReturnsCovariances @ wlsFactorMimickingPortfolios @ np.linalg.inv(wlsFactorMimickingPortfolios.T @ assetLogExcessReturnsCovariances @ wlsFactorMimickingPortfolios); wlsRecoveredBetas.columns = betas.columns

# My approach to constructing factors
myWeights = assetLogExcessReturnsCovariancesInverse
myFactorMimickingPortfolios = constructFactorMimickingPortfolios(myWeights, betas, assetLogExcessReturns)
myRecoveredBetas = assetLogExcessReturnsCovariances @ myFactorMimickingPortfolios @ np.linalg.inv(myFactorMimickingPortfolios.T @ assetLogExcessReturnsCovariances @ myFactorMimickingPortfolios); myRecoveredBetas.columns = betas.columns
