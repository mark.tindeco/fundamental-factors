from asset import Asset
from datasources import DatabaseFinancial

adjusted = True

# ['III.L', 'ADML.L', 'AAL.L', 'ANTO.L', 'AHT.L', 'ABF.L', 'AZN.L', 'AUTOA.L', 'AVV.L', 'AV.L', 'BAES.L', 'BARC.L', 'BDEV.L', 'BKGH.L', 'BHPB.L', 'BP.L', 'BATS.L', 'BLND.L', 'BT.L', 'BNZL.L', 'BRBY.L', 'CCL.L', 'CNA.L', 'CCH.L', 'CPG.L', 'CRH.L', 'CRDA.L', 'DCC.L', 'DGE.L', 'EVRE.L', 'EXPN.L', 'FERG.L', 'FLTRF.L', 'FRES.L', 'GSK.L', 'GLEN.L', 'HLMA.L', 'HRGV.L', 'HSX.L', 'HSBA.L', 'HIK.L', 'IMB.L', 'INF.L', 'IHG.L', 'ICAG.L', 'ITRK.L', 'ITV.L', 'JD.L', 'JMAT.L', 'KGF.L', 'LAND.L', 'LGEN.L', 'LLOY.L', 'LSE.L', 'MNG.L', 'MGGT.L', 'MRON.L', 'MNDI.L', 'MRW.L', 'NG.L', 'NXT.L', 'NMC.L', 'OCDO.L', 'PSON.L', 'PSN.L', 'PHNX.L', 'POLYP.L', 'PRU.L', 'RB.L', 'REL.L', 'RTO.L', 'RIO.L', 'RMV.L', 'RR.L', 'RBS.L', 'RDSa.L', 'RSA.L', 'SGE.L', 'SBRY.L', 'SDR.L', 'SMT.L', 'SGRO.L', 'SVT.L', 'SN.L', 'SMDS.L', 'SMIN.L', 'SKG.L', 'SPX.L', 'SSE.L', 'STAN.L', 'SLA.L', 'SJP.L', 'TW.L', 'TSCO.L', 'TUIT.L', 'ULVR.L', 'UU.L', 'VOD.L', 'WTB.L', 'WPP.L']

rics = ['AAL.L', 'AUTOA.L']
with DatabaseFinancial('historical_prices', 3307) as historical_prices:
    asset_ids = historical_prices.get_asset_ids(rics=rics)
    for asset_id in asset_ids:
        asset = Asset(asset_id, historical_prices)
        all_prices = asset.get_all_prices(adjusted=adjusted)
        asset.plot(adjusted=adjusted)
